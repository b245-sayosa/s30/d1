//[SECTION] MongoDB aggregation

/*
	-to generate and perform operation to create filtered results that helps us analyze the data.
*/

/*using aggregate method: 
	-the "$match" is used to pass the documents that meet the specified condition

	conditions to the next stage or aggregation process

	syntax: 

	{$match : {Field: value}}
*/

db.fruits.aggregate.([ {$match : {onSale: true}}]);

/*
	-The group is used to group the elements together and field-value the data from the grouped element.


	syntax:
	{$group: _id: "fieldSetGroup"}
*/

//separates yellow and shows only supplier id and total stock
db.fruits.aggregate([ 
  {$match : {color: "Yellow"}},
  {$group:{_id:"$supplier_id",totalFruits: {$sum: "$stock"}}}
]);

//mini activity solution

db.fruits.aggregatte.([
		{
			$match: {color: "Yellow"}
		},
		{
			$group:{
				_id: "$supplier_id",totalFruits: {$sum: "stocks"}
			}
		}
	])

//Field Projection with aggregation
/*
		-$project can be used when aggregation data to include/exclude from the returned result

		syntax:{$sort: {field: value}}

	*/

db.fruits.aggregate([ 
  {$match : {onSale: true}},
  {$group:{_id:"$supplier_id",totalFruits: {$sum: "$stock"}}},{$project: {_id:0}}
  {$sort: {totalFruits:1}}
]);

// Sorting aggregated results
	/*
		-$sort can be used to change the order of the aggregated result

	*/

/*sortting value
		1-lowest to highest
		-1-highest to lowest

*/

//aggregating results based on an array fields 

	/*
		the $unwind deconstructs an array field from a collection field with an array value to output a result

	*/

//sum
db.fruits.aggregate([
{
	$unwind: "$origin"
},
{
	$group: {_id:"$origin", fruits{$sum:1}}
}

	]);
//stocks
db.fruits.aggregate([
{
	$unwind: "$origin"
},
{
	$group: {_id:"$origin", fruits:{$sum: "$stock"}}
}

	]);


db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"}

	]);

